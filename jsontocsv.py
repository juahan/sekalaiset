import csv
import json

y="""{
        "locations" : [ 
        {   "timestampMs" : "1404555110001",
            "latitudeE7" : 696742917,
            "longitudeE7" : 189666421,
            "accuracy" : 25,
    		"fields": 
        	{ 	"codename": "add_logentry", 
        		"name": "Can add log entry", 
        		"content_type": 8 
        	} 

    	}, 
    	{	"timestampMs" : "1404555049911",
    		"latitudeE7" : 696742599,
    		"longitudeE7" : 189666096,
    		"accuracy" : 22
    	}, 
    	{	"timestampMs" : "1404554947043",
    		"latitudeE7" : 696742917,
    		"longitudeE7" : 189666421,
    		"accuracy" : 25	
    	}, 
    	{	"timestampMs" : "1404554887002",
    		"latitudeE7" : 696742894,
    		"longitudeE7" : 189666304,
    		"accuracy" : 25
    	} 
    	]
	}"""

inputTiedostonimi="LocationHistory.json"
#inputTiedostonimi="head100.txt"

outputTiedostonimi="LocationHistory.csv"

with open (inputTiedostonimi,"r") as myfile:
    data=myfile.read().replace('\n', '')

x = json.loads(data)

f = csv.writer(open(outputTiedostonimi, "wb+"))

# Write CSV Header, If you dont need that, remove this line
f.writerow(["timestampMs", "latitudeE7", "longitudeE7", "accuracy"])

kk=0
for x in x["locations"]:
	for y in x:
		f.writerow([x["timestampMs"], x["latitudeE7"]/10000000.0, x["longitudeE7"]/10000000.0, x["accuracy"]])
		kk=kk+1
		if kk%1000==0:
			print("Kirjoitettu: {}").format(kk)