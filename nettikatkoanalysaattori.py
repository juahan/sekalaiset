import time
import datetime
import collections

# TODO: Keskimääräinen ping jne esim

# Seuraava komento ajelemaan terminaaliin:
# ping google.com | while read pong; do echo "$(date "+%d.%m.%Y %H:%M:%S"): $pong"; done > ~/log.txt
# Eli pingaa googlea ja tallentaa aikaleimalliset tiedot kotihakemistoon lo.txt -tiedostoon

# Esimerkki tiedostosta:
# 24.12.2015 10:10:17: 64 bytes from 62.78.98.227: icmp_seq=560 ttl=58 time=8.181 ms
# 24.12.2015 10:10:18: 64 bytes from 62.78.98.227: icmp_seq=561 ttl=58 time=10.245 ms
# 24.12.2015 10:10:20: Request timeout for icmp_seq 562
# 24.12.2015 10:10:21: Request timeout for icmp_seq 563


def get_time(string_time):
    string_time = string_time.split(": ")[0]
    good_time = time.strptime(string_time, "%d.%m.%Y %H:%M:%S")
    return good_time

lokitiedosto = "/Users/juhana/log.txt"
kirjoitustiedosto = "/Users/juhana/log_analyysi.txt"
katkojen_pituudet_tiedosto = "/Users/juhana/log_analyysi_pituudet.txt"

f = open(lokitiedosto, "r")
f2 = open(kirjoitustiedosto, "w")
print("Katkoanalyysi:")
print("(katkoja - katkon pituus - katkojen yhteispituus - edellisen alkuun")
print("-----------------------------------\t")
f2.write("Katkoanalyysi:\n")
f2.write("katkoja - katkon pituus - katkojen yhteispituus - edellisen alkuun\n")
f2.write("-----------------------------------\n")
jono = collections.deque(maxlen=2)
lines = f.readlines()
start = False
katkoja = 1
pituus_yhteensa = None
katkojen_vali = datetime.timedelta(0, 0)
katkojen_pituudet = []
katkojen_valit = []
for line in lines:
    line_clean = line.strip()
    if "Request timeout" in line_clean and not start:
        alku = get_time(line_clean)
        jono.append(alku)
        alku_f = time.strftime("%d.%m.%Y %H:%M:%S", alku)
        print("Katkon alkaa:\t" + str(alku_f))
        f2.write("Katkon alkaa:\t" + str(alku_f) + "\n")
        start = True
        loppu = None
    if "64 bytes from" in line_clean and start:
        loppu = get_time(line_clean)
        if len(jono) > 1:
            edellisen_alku = jono.popleft()
            katkojen_vali = (datetime.datetime(*alku[:6]) - datetime.datetime(*edellisen_alku[:6])) + datetime.timedelta(0, 1)
            katkojen_valit.append(katkojen_vali)
        loppu_f = time.strftime("%d.%m.%Y %H:%M:%S", loppu)
        pituus = (datetime.datetime(*loppu[:6]) - datetime.datetime(*alku[:6])) + datetime.timedelta(0,1)
        if pituus_yhteensa is None:
            pituus_yhteensa = pituus
            katkojen_pituudet.append(str(pituus))
        else:
            pituus_yhteensa += pituus
            katkojen_pituudet.append(str(pituus))
        print("Katkon loppuu:\t" + str(loppu_f), end="\t")
        print(str(katkoja) + " - " + str(pituus) + " - " + str(pituus_yhteensa) + " - " + str(katkojen_vali) + "\n")
        f2.write("Katkon loppuu:\t" + str(loppu_f) + "\t")
        f2.write(str(katkoja) + " - " + str(pituus) + " - " + str(pituus_yhteensa) + " - " + str(katkojen_vali) + "\n\n")
        start = False
        alku = None
        katkoja += 1

f.close()
f2.close()

f = open(katkojen_pituudet_tiedosto, "w")
for a, b in zip(katkojen_pituudet, katkojen_valit):
    f.write(str(a))
    f.write(",")
    f.write(str(b))
    f.write("\n")
f.close()
